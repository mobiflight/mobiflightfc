# This repository has moved #
The Mobiflight Project has moved their repository over to GitHub. Development only happens on the [new MobiFlight GitHub repository](https://github.com/Mobiflight/MobiFlight-Connector).

## New Repository URL ##
The new repostory is at (https://github.com/Mobiflight/MobiFlight-Connector)

## New Issue Tracker ##
Report bugs or improvements on the [new tracker](https://github.com/Mobiflight/MobiFlight-Connector/issues)

## Project Website ##
For more information also take a look at the website at (https://mobiflight.com)

## Who do I talk to? ##
Sebastian aka DocMoebiuz (on Github, or Discord, or the forums)